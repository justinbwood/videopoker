﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace VideoPoker
{
    public class Hand
    {
        public List<Card> CurrentHand { get; private set; }
        public int HandSize { get; private set; }

        public Hand(List<Card> cards, int size)
        {
            CurrentHand = new List<Card>();
            HandSize = size;
            NewHand(cards);
        }

        public List<Card> ToggleHold(int index)
        {
            CurrentHand[index].IsHeld = !CurrentHand[index].IsHeld;
            Debug.WriteLine(string.Format("Held/Unheld card: {0}", CurrentHand[index]));
            return CurrentHand;
        }

        public int CardsHeld()
        {
            return CurrentHand.Count(card => card.IsHeld);
        }

        public int CardsNotHeld()
        {
            return CurrentHand.Count(card => !card.IsHeld);
        }

        public List<Card> NewHand(List<Card> cards)
        {
            CurrentHand.ForEach(card => card.IsHeld = false);
            CurrentHand.Clear();
            CurrentHand.AddRange(cards.GetRange(0, HandSize));
            return CurrentHand;
        }

        public List<Card> DrawCards(List<Card> cards)
        {
            for (int i = 0; i < CurrentHand.Count; i++)
            {
                if (!CurrentHand[i].IsHeld)
                {
                    Debug.WriteLine(string.Format("Threw away: {0}", CurrentHand[i]));
                    CurrentHand[i] = cards[0];
                    cards.RemoveAt(0);
                }
            }
            return CurrentHand;
        }
    }
}
