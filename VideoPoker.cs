﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;

namespace VideoPoker
{
    public enum DoubleDoubleBonusHands { None, JacksOrBetter, TwoPair, ThreeOfAKind, Straight, Flush, FullHouse, FourFiveThroughKing, FourTwoThroughFour, FourAces, FourTwoThroughFourKicker, FourAcesKicker, StraightFlush, RoyalFlush }

    public abstract class PokerGame
    {
        public ReadOnlyDictionary<IConvertible, Func<IEnumerable<Card>, bool>> Rules { get; set; }
        public Hand PokerHand { get; set; }
        public Deck PokerDeck { get; set; }
        public int HandSize { get; set; }

        public bool NewTurn { get; set; }

        public abstract IConvertible EvalHand();
        public abstract List<Card> TakeTurn();
    }

    public class DoubleDoubleBonusPokerGame : PokerGame
    {

        public DoubleDoubleBonusPokerGame()
        {
            Func<IEnumerable<Card>, bool> hasPair =
                cards => cards.GroupBy(card => card.CardRank)
                .Count(group => group.Count() == 2) == 1;

            Func<IEnumerable<Card>, bool> isPair =
                cards => cards.GroupBy(card => card.CardRank)
                .Count(group => group.Count() == 3) == 0
                && hasPair(cards);

            Func<IEnumerable<Card>, bool> isTwoPair =
                cards => cards.GroupBy(card => card.CardRank)
                .Count(group => group.Count() == 2) == 2;

            Func<IEnumerable<Card>, bool> isJacksOrBetter =
                cards => isPair(cards)
                && (cards.GroupBy(card => card.CardRank).OrderByDescending(group => group.Count()).First().Key >= Rank.Jack
                || cards.GroupBy(card => card.CardRank).OrderByDescending(group => group.Count()).First().Key == Rank.Ace);

            Func<IEnumerable<Card>, bool> hasThreeOfKind =
                cards => cards.GroupBy(card => card.CardRank)
                .Any(group => group.Count() == 3);

            Func<IEnumerable<Card>, bool> isThreeOfKind =
                cards => hasThreeOfKind(cards) && !hasPair(cards);

            Func<IEnumerable<Card>, bool> isFullHouse =
                cards => hasPair(cards) && hasThreeOfKind(cards);

            Func<IEnumerable<Card>, bool> hasFourOfKind =
                cards => cards.GroupBy(card => card.CardRank)
                .Any(group => group.Count() == 4);

            Func<IEnumerable<Card>, bool> hasFourAces =
                cards => hasFourOfKind(cards)
                && cards.GroupBy(card => card.CardRank).OrderByDescending(group => group.Count()).First().Key == Rank.Ace;

            Func <IEnumerable<Card>, bool> hasFourLow =
                cards => hasFourOfKind(cards)
                && cards.GroupBy(card => card.CardRank).OrderByDescending(group => group.Count()).First().Key >= Rank.Two
                && cards.GroupBy(card => card.CardRank).OrderByDescending(group => group.Count()).First().Key <= Rank.Four;

            Func<IEnumerable<Card>, bool> isFourOfKindHigh =
                cards => hasFourOfKind(cards)
                && cards.GroupBy(card => card.CardRank).OrderByDescending(group => group.Count()).First().Key >= Rank.Five;

            Func<IEnumerable<Card>, bool> isFourAcesWithKicker =
                cards => hasFourAces(cards)
                && cards.GroupBy(card => card.CardRank).OrderBy(group => group.Count()).First().Key >= Rank.Ace
                && cards.GroupBy(card => card.CardRank).OrderBy(group => group.Count()).First().Key <= Rank.Four;

            Func<IEnumerable<Card>, bool> isFourAces =
                cards => hasFourAces(cards)
                && !isFourAcesWithKicker(cards);

            Func<IEnumerable<Card>, bool> isFourOfKindLowWithKicker =
                cards => hasFourLow(cards)
                && cards.GroupBy(card => card.CardRank).OrderBy(group => group.Count()).First().Key >= Rank.Ace
                && cards.GroupBy(card => card.CardRank).OrderBy(group => group.Count()).First().Key <= Rank.Four;

            Func<IEnumerable<Card>, bool> isFourOfKindLow =
                cards => hasFourLow(cards)
                && !isFourOfKindLowWithKicker(cards);

            Func<IEnumerable<Card>, bool> hasStraight =
                cards => cards.GroupBy(card => card.CardRank)
                .Count() == cards.Count()
                && (cards.Max(card => (int)card.CardRank) - cards.Min(card => (int)card.CardRank) == 4
                || cards.All(card => card.CardRank == Rank.Ace || card.CardRank >= Rank.Ten));

            Func <IEnumerable<Card>, bool> hasFlush =
                cards => cards.GroupBy(card => card.CardSuit).Count() == 1;

            Func<IEnumerable<Card>, bool> hasStraightFlush =
                cards => hasFlush(cards) && hasStraight(cards);

            Func<IEnumerable<Card>, bool> isRoyalFlush =
                cards => cards.Min(card => card.CardRank) == Rank.Ten
                && hasStraightFlush(cards);

            Func<IEnumerable<Card>, bool> isStraightFlush =
                cards => hasStraightFlush(cards) && !isRoyalFlush(cards);

            Func<IEnumerable<Card>, bool> isStraight =
                cards => hasStraight(cards)
                && !isStraightFlush(cards);

            Func<IEnumerable<Card>, bool> isFlush =
                cards => hasFlush(cards)
                && !isStraightFlush(cards);

            Rules = new ReadOnlyDictionary<IConvertible, Func<IEnumerable<Card>, bool>>(new Dictionary<IConvertible, Func<IEnumerable<Card>, bool>>
            {
                { DoubleDoubleBonusHands.JacksOrBetter, isJacksOrBetter },
                { DoubleDoubleBonusHands.TwoPair, isTwoPair },
                { DoubleDoubleBonusHands.ThreeOfAKind, isThreeOfKind },
                { DoubleDoubleBonusHands.Straight, isStraight },
                { DoubleDoubleBonusHands.Flush, isFlush },
                { DoubleDoubleBonusHands.FullHouse, isFullHouse },
                { DoubleDoubleBonusHands.FourFiveThroughKing, isFourOfKindHigh },
                { DoubleDoubleBonusHands.FourTwoThroughFour, isFourOfKindLow },
                { DoubleDoubleBonusHands.FourAces, isFourAces },
                { DoubleDoubleBonusHands.FourTwoThroughFourKicker, isFourOfKindLowWithKicker },
                { DoubleDoubleBonusHands.FourAcesKicker, isFourAcesWithKicker },
                { DoubleDoubleBonusHands.StraightFlush, isStraightFlush },
                { DoubleDoubleBonusHands.RoyalFlush, isRoyalFlush }
            });

            PokerDeck = new Deck();
            HandSize = 5;
            PokerHand = new Hand(PokerDeck.Deal(HandSize), HandSize);
            NewTurn = true;

        }

        public override IConvertible EvalHand()
        {
            IConvertible result = DoubleDoubleBonusHands.None;
            foreach (var rule in Rules)
            {
                if (rule.Value(PokerHand.CurrentHand))
                {
                    result = rule.Key;
                }
            }
            return result;
        }

        public override List<Card> TakeTurn()
        {
            List<Card> newHand = new List<Card>();
            if (NewTurn)
            {
                newHand = PokerHand.DrawCards(PokerDeck.Deal(PokerHand.CardsNotHeld()));
                Debug.WriteLine(string.Format("Hand after draw: {0}", string.Join(", ", newHand)));
            }
            else
            {
                PokerDeck.ResetDeck();
                newHand = PokerHand.NewHand(PokerDeck.Deal(HandSize));
                Debug.WriteLine(string.Format("Hand after deal: {0}", string.Join(", ", newHand)));
            }
            NewTurn = !NewTurn;
            Debug.WriteLine(string.Format("Cards in deck: {0}", PokerDeck.TotalCards()));
            return newHand;
        }
    }

    public class VideoPokerController
    {
        public VideoPokerController()
        {

        }

        public PokerGame NewDoubleDoubleBonusPokerGame()
        {
            return new DoubleDoubleBonusPokerGame();
        }
    }
}
