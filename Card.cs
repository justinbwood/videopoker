﻿using System;

namespace VideoPoker
{
    public enum Suit { Clubs, Diamonds, Hearts, Spades }
    public enum Rank { Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King }

    public class Card
    {
        public Suit CardSuit { get; private set; }
        public Rank CardRank { get; private set; }
        public string ImagePath { get; private set; }
        public bool IsHeld { get; set; }

        public Card(Suit s, Rank r)
        {
            CardSuit = s;
            CardRank = r;
            IsHeld = false;
            ImagePath = string.Format("/Images/{0}_{1}.png", Enum.GetName(typeof(Rank), CardRank), Enum.GetName(typeof(Suit), CardSuit));
        }

        public override string ToString()
        {
            return string.Format("{0} of {1}", Enum.GetName(typeof(Rank), CardRank), Enum.GetName(typeof(Suit), CardSuit));
        }
    }
}
