﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VideoPoker
{
    public class Deck
    {
        private static Random rng = new Random();

        private List<Card> OriginalDeck { get; set; }
        private List<Card> Cards { get; set; }

        public Deck()
        {
            Cards = new List<Card>();
            OriginalDeck = new List<Card>();
            Array.ForEach(Enum.GetValues(typeof(Suit)).Cast<Suit>().ToArray(), s => Array.ForEach(Enum.GetValues(typeof(Rank)).Cast<Rank>().ToArray(), r => OriginalDeck.Add(new Card(s, r))));
            ResetDeck();
        }

        private void Shuffle()
        {
            Cards = Cards.OrderBy(card => rng.Next()).ToList();
        }

        public void ResetDeck()
        {
            Cards.Clear();
            Cards = new List<Card>(OriginalDeck);
            Shuffle();
        }

        public int TotalCards()
        {
            return Cards.Count;
        }

        public List<Card> Deal(int n)
        {
            Shuffle();
            var hand = Cards.Take(n).ToList();
            Cards.RemoveRange(0, n);
            return hand;
        }
    }
}
