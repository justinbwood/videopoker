﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;

namespace VideoPoker
{
    public class VideoPokerViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public VideoPokerController Controller { get; set; }
        public PokerGame Game { get; set; }
        public Deck PokerDeck { get; set; }

        private ObservableCollection<Card> _hand;
        public ObservableCollection<Card> Hand
        {
            get { return _hand; }
            set
            {
                _hand = value;
                NotifyPropertyChanged("Hand");
            }
        }

        private string _handString;
        public string HandString
        {
            get { return _handString; }
            set
            {
                _handString = value;
                NotifyPropertyChanged("HandString");
            }
        }

        private string _resultString;
        public string ResultString
        {
            get { return _resultString; }
            set
            {
                _resultString = value;
                NotifyPropertyChanged("ResultString");
            }
        }

        private RelayCommand _newHandCommand;
        public RelayCommand NewHandCommand
        {
            get
            {
                if (_newHandCommand == null)
                {
                    _newHandCommand = new RelayCommand(o => NewHand(), o => true);
                }
                return _newHandCommand;
            }
        }

        private RelayCommand _holdCard;
        public RelayCommand HoldCard
        {
            get
            {
                if (_holdCard == null)
                {
                    _holdCard = new RelayCommand(o => ToggleHold(o), o => true);
                }
                return _holdCard;
            }
        }

        private void ToggleHold(object o)
        {
            Hand.Clear();
            Game.PokerHand.ToggleHold(Convert.ToInt32(o)).ForEach(Hand.Add);
        }

        private void NewHand()
        {
            Hand.Clear();
            Game.TakeTurn().ForEach(Hand.Add);
            HandString = string.Join(", ", Hand);
            ResultString = Game.EvalHand().ToString();
        }

        public VideoPokerViewModel()
        {
            Controller = new VideoPokerController();
            Game = Controller.NewDoubleDoubleBonusPokerGame();
            Hand = new ObservableCollection<Card>();
            Game.PokerHand.CurrentHand.ForEach(Hand.Add);

            //Debug.WriteLine(string.Join(Environment.NewLine, Assembly.GetExecutingAssembly().GetManifestResourceNames()));
        }
    }
}
